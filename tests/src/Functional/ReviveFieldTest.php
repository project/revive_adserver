<?php

namespace Drupal\Tests\revive_adserver\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\revive_adserver\Traits\ReviveTestTrait;

/**
 * Tests adding a revive adserver field to an entity.
 *
 * @group revive_adserver
 */
class ReviveFieldTest extends BrowserTestBase {

  use ReviveTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'revive_adserver',
  ];

  /**
   * A user with permissions to edit Articles.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $contentAuthorUser;

  /**
   * The node object used in the test.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create the Article node type.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Log in as a content author who can create articles and use the revive
    // field.
    $this->contentAuthorUser = $this->drupalCreateUser([
      'access content',
      'create article content',
      'edit any article content',
      'delete any article content',
      'use revive_adserver field',
      'administer revive_adserver',
    ]);
    $this->drupalLogin($this->contentAuthorUser);

    // Setup initial revive configuration.
    $this->configureModule();
    $this->setupAdZones();
  }

  /**
   * Tests, that adding a revive field, without invocation method per entity.
   */
  public function testReviveFieldWithoutInvocationMethodPerEntity() {
    $this->setupReviveField();

    // Create a test node.
    $this->node = $this->createNode([
      'type' => 'article',
      'field_revive' => ['zone_id' => 1],
    ]);

    // Open the node.
    $this->drupalGet('node/' . $this->node->id());

    // Verify, that we have an embedded async revive ad zone.
    $this->assertSession()
      ->elementAttributeContains('css', '.field--name-field-revive ins', 'data-revive-zoneid', '1');
    $this->assertSession()->elementExists('css', '.field--name-field-revive script');
  }

  /**
   * Tests, that adding a revive field, with invocation method per entity.
   */
  public function testReviveFieldWithInvocationMethodPerEntity() {
    $withInvocationMethodPerEntity = TRUE;
    $this->setupReviveField($withInvocationMethodPerEntity);

    // Create a test node.
    $this->node = $this->createNode([
      'type' => 'article',
      'field_revive' => ['zone_id' => 1, 'invocation_method' => 'iframe'],
    ]);

    // Open the node.
    $this->drupalGet('node/' . $this->node->id());

    // Verify, that we have an embedded iframe revive ad zone.
    $this->assertSession()->elementExists('css', '.field--name-field-revive iframe');
  }

}
