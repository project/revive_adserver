<?php

namespace Drupal\Tests\revive_adserver\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\revive_adserver\Traits\ReviveTestTrait;

/**
 * Tests the Revive Adserver module block functionality.
 *
 * @group revive_adserver
 */

class ReviveBlockJavascriptTest extends WebDriverTestBase {

  use ReviveTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'revive_adserver',
  ];

  /**
   * A user with permissions to access the revive adserver settings page.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Log in as a user, that can add and configure blocks.
    $this->user = $this->drupalCreateUser([
      'administer revive_adserver',
      'administer blocks',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Test adding an revive adserver block creates the expected markup.
   */
  public function testAddReviveBlock() {
    // Add ad zones.
    $this->setupAdZones();

    // Add a revive block using the iframe method.
    $edit = [
      'settings[zone_id]' => 1,
      'settings[invocation_method]' => 'iframe',
      'region' => 'content',
    ];
    $this->drupalGet('admin/structure/block/add/revive_adserver_zone_block');
    $this->submitForm($edit, 'Save block');

    // Check that the ad wrappers and iframe got loaded.
    $this->drupalGet('<front>');
    $this->assertSession()->elementExists('css', '.block-revive-adserver iframe');

    // Change invocation method to "async_javascript" and check the markup.
    $edit = [
      'settings[invocation_method]' => 'async_javascript',
    ];
    $this->drupalGet('admin/structure/block/manage/reviveadserverzoneblock');
    $this->submitForm($edit, 'Save block');
    $this->assertSession()->elementExists('css', '.block-revive-adserver ins');
    $this->assertSession()->elementAttributeContains('css', '.block-revive-adserver ins', 'data-revive-zoneid', 1);

    // Change block banner option.
    $edit = [
      'settings[block_banner]' => TRUE,
    ];
    $this->drupalGet('admin/structure/block/manage/reviveadserverzoneblock');
    $this->submitForm($edit, 'Save block');
    $this->assertSession()->elementExists('css', '.block-revive-adserver ins');
    $this->assertSession()->elementAttributeContains('css', '.block-revive-adserver ins', 'data-revive-block', 1);

    // Change block banner campaign option.
    $edit = [
      'settings[block_banner_campaign]' => TRUE,
    ];
    $this->drupalGet('admin/structure/block/manage/reviveadserverzoneblock');
    $this->submitForm($edit, 'Save block');
    $this->assertSession()->elementExists('css', '.block-revive-adserver ins');
    $this->assertSession()->elementAttributeContains('css', '.block-revive-adserver ins', 'data-revive-blockcampaign', 1);

    // Change invocation method to "javascript" and check the markup.
    $edit = [
      'settings[invocation_method]' => 'javascript',
    ];
    $this->drupalGet('admin/structure/block/manage/reviveadserverzoneblock');
    $this->submitForm($edit, 'Save block');
    $this->assertSession()->elementExists('css', '.block-revive-adserver script');
  }

}