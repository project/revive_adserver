<?php

namespace Drupal\Tests\revive_adserver\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\revive_adserver\Traits\ReviveTestTrait;

/**
 * Tests the Revive Adserver module field functionality.
 *
 * @group revive_adserver
 */
class ReviveFieldJavascriptTest extends WebDriverTestBase {

  use ReviveTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'node',
    'revive_adserver',
  ];

  /**
   * A user with permissions to access the revive adserver settings page.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article']);
    $this->setupFields();

    // Log in as a user, that can add and configure blocks.
    $this->user = $this->drupalCreateUser([
      'create article content',
      'edit any article content',
      'administer nodes',
      'administer node fields',
      'administer node display',
      'administer node form display',
      'administer revive_adserver',
      'use revive_adserver field',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Test adding an revive ad via a field.
   */
  public function testAddReviveField() {
    // Add ad zones.
    $this->setupAdZones();

    // Test the revive field with default settings.
    $this->drupalGet('node/add/article');
    $edit = [
      'title[0][value]' => 'My test title',
      'field_revive[0][zone_id]' => 1,
    ];
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');

    // Verify that the ad is displayed correctly. By default with the "async_javascript"
    // method.
    $this->assertSession()->elementExists('css', '.field--name-field-revive ins');
    $this->assertSession()->elementAttributeContains('css', '.field--name-field-revive ins', 'data-revive-zoneid', 1);

    // Change the block banner option.
    $this->drupalGet('admin/structure/types/manage/article/display');
    $this->click('[name="field_revive_settings_edit"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->checkField('fields[field_revive][settings_edit_form][settings][block_banner]');
    $this->getSession()->getPage()->findButton('Update')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->findButton('Save')->click();
    $this->drupalGet('node/1');
    $this->assertSession()->elementAttributeContains('css', '.field--name-field-revive ins', 'data-revive-block', 1);

    // Change the block banner campaign option.
    $this->drupalGet('admin/structure/types/manage/article/display');
    $this->click('[name="field_revive_settings_edit"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->checkField('fields[field_revive][settings_edit_form][settings][block_banner_campaign]');
    $this->getSession()->getPage()->findButton('Update')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->findButton('Save')->click();
    $this->drupalGet('node/1');
    $this->assertSession()->elementAttributeContains('css', '.field--name-field-revive ins', 'data-revive-blockcampaign', 1);

    // Change invocation method to "iframe".
    $this->drupalGet('admin/structure/types/manage/article/display');
    $this->click('[name="field_revive_settings_edit"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->fillField('fields[field_revive][settings_edit_form][settings][invocation_method]', 'iframe');
    $this->getSession()->getPage()->findButton('Update')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->findButton('Save')->click();

    // Verify that the rendered markup uses the "iframe" invocation method.
    $this->drupalGet('node/1');
    $this->assertSession()->elementExists('css', '.field--name-field-revive iframe');
  }

  /**
   * Tests that only the whitelisted ad zones available when being configured.
   */
  public function testWhitelistingAdZones() {
    // Add ad zones.
    $this->setupAdZones();

    // Whitelist a single ad zone.
    $this->drupalGet('admin/structure/types/manage/article/form-display');
    $this->click('[name="field_revive_settings_edit"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->selectFieldOption('fields[field_revive][settings_edit_form][settings][enabled_zones][]', 2);
    $this->getSession()->getPage()->findButton('Update')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->findButton('Save')->click();

    // Add a node and check, that only the whitelisted ad unit is available.
    $this->drupalGet('node/add/article');
    $this->assertSession()->elementsCount('css', '[name="field_revive[0][zone_id]"] option', 1);
    $this->assertSession()->optionExists('field_revive[0][zone_id]', 2);

    // Whitelist all ad units.
    $this->drupalGet('admin/structure/types/manage/article/form-display');
    $this->click('[name="field_revive_settings_edit"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->selectFieldOption('fields[field_revive][settings_edit_form][settings][enabled_zones][]', 1, TRUE);
    $this->getSession()->getPage()->selectFieldOption('fields[field_revive][settings_edit_form][settings][enabled_zones][]', 2, TRUE);
    $this->getSession()->getPage()->findButton('Update')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->findButton('Save')->click();

    // Add a node and check, that all ad units are available.
    $this->drupalGet('node/add/article');
    $this->assertSession()->elementsCount('css', '[name="field_revive[0][zone_id]"] option', 2);
    $this->assertSession()->optionExists('field_revive[0][zone_id]', 1);
    $this->assertSession()->optionExists('field_revive[0][zone_id]', 2);
  }

  /**
   * Test that the invocation method per entity option works as expected.
   */
  public function testInvocationMethodPerEntityOption() {
    // Add ad zones.
    $this->setupAdZones();

    // Enable the invocation method per entity option.
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_revive');
    $this->getSession()->getPage()->selectFieldOption('default_value_input[field_revive][0][zone_id]', 1);
    $this->getSession()->getPage()->checkField('settings[invocation_method_per_entity]');
    $this->getSession()->getPage()->findButton('Save settings')->click();

    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.field_revive');

    // Add a node and check, that the invocation method can be configured.
    $this->drupalGet('node/add/article');
    $this->getSession()->getPage()->fillField('title[0][value]', 'My test title');
    $this->assertSession()->elementExists('css', '[name="field_revive[0][invocation_method]"]');
    $this->getSession()->getPage()->selectFieldOption('field_revive[0][invocation_method]', 'iframe');
    $this->getSession()->getPage()->findButton('Save')->click();

    // Check that the selected invocation method was used.
    $this->assertSession()->elementExists('css', '.field--name-field-revive iframe');
  }

  /**
   * Creates the necessary fields.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setupFields() {
    FieldStorageConfig::create([
      'field_name' => 'field_revive',
      'entity_type' => 'node',
      'type' => 'revive_adserver_zone',
      'cardinality' => 1,
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_revive',
      'bundle' => 'article',
      'label' => 'Revive Adserver',
    ])->save();
    EntityFormDisplay::load('node.article.default')
      ->setComponent('field_revive')
      ->save();
    EntityViewDisplay::load('node.article.default')
      ->setComponent('field_revive')
      ->save();
  }

}