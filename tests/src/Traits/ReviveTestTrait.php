<?php

namespace Drupal\Tests\revive_adserver\Traits;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Provides common helper for the revive adserver test classes.
 */
trait ReviveTestTrait {

  /**
   * Configures the module with a test endpoint.
   */
  public function configureModule() {
    // Setup initial revive configuration.
    $this->drupalGet('admin/config/services/revive-adserver');
    $edit = [
      'delivery_url' => 'ads.myserver.local/delivery',
      'delivery_url_ssl' => 'ads.myserver.local/delivery',
      'publisher_id' => 1,
    ];
    $this->submitForm($edit, 'Save');
  }

  /**
   * Setup the revive adserver zones.
   */
  public function setupAdZones() {
    // Zones can be only imported, not specified in the form. So set them
    // manually for testing purposes.
    $config = \Drupal::configFactory()->getEditable('revive_adserver.settings');
    $config->set('zones', [
      [
        'id' => 1,
        'name' => 'Skyscraper',
        'width' => 120,
        'height' => 600,
      ],
      [
        'id' => 2,
        'name' => 'Classic Banner',
        'width' => 468,
        'height' => 60,
      ],
    ]);
    $config->save();
  }

  /**
   * Creates a revive field without invocation method per entity selection.
   *
   * @param bool $withInvocationMethodPerEntity
   *   Configures the field with invocation method per entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function setupReviveField($withInvocationMethodPerEntity = FALSE) {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_revive',
      'entity_type' => 'node',
      'type' => 'revive_adserver_zone',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'article',
      'settings' => ['invocation_method_per_entity' => $withInvocationMethodPerEntity],
    ]);
    $field->save();
    EntityFormDisplay::load('node.article.default')
      ->setComponent('field_revive', [])
      ->save();
    // Don't specify the invocation method in the field formatter.
    EntityViewDisplay::load('node.article.default')
      ->setComponent('field_revive', [])
      ->save();
  }

}
